import express from 'express';
import * as studentsService from './students.service';
import { ValidatedRequest } from 'express-joi-validation';
import { IstudentCreateRequest } from './types/student-create-request.interface';
import { IstudentUpdateRequest } from './types/student-update-request';

const getAllStudents = async (req: express.Request, res: express.Response) => {
    const students = await studentsService.getAllStudents();

    res.status(200).json(students);
};

const getStudentById = async (req: express.Request, res: express.Response) => {
    const studentId = req.params.id;
    const student = await studentsService.getStudentById(studentId);
    res.status(200).json(student);
};

const addNewStudent = async (
    req: ValidatedRequest<IstudentCreateRequest>,
    res: express.Response
) => {
    const data = req.body;
    const newStudent = await studentsService.addNewStudent(data);
    res.status(201).json(newStudent);
};

const updateStudentById = async (
    req: ValidatedRequest<IstudentUpdateRequest>,
    res: express.Response
) => {
    const studentId = req.params.id;
    const info = req.body;
    const updatedStudent = await studentsService.updateStudentById(
        studentId,
        info
    );
    res.status(200).json(updatedStudent);
};

const addImage = async (
    req: express.Request<{ id: string; file: Express.Multer.File }>,
    res: express.Response
) => {
    const studentId = req.params.id;
    const { path } = req.file ?? {};
    const updatedStudent = await studentsService.addImage(studentId, path);
    res.status(200).json(updatedStudent);
};

const updateStudentGroup = async (
    req: express.Request<{
        groupId: string;
        id: string;
    }>,
    res: express.Response
) => {
    const studentId = req.params.id;
    const groupId = req.body;

    const updatedStudent = await studentsService.updateStudentById(
        studentId,
        groupId
    );
    res.status(200).json(updatedStudent);
};

const deleteStudentById = async (
    req: express.Request<{ id: string }>,
    res: express.Response
) => {
    const studentId = req.params.id;
    await studentsService.deleteStudentById(studentId);
    res.status(200).json({ message: 'student successfully deleted' });
};

export {
    getAllStudents,
    getStudentById,
    addNewStudent,
    updateStudentById,
    addImage,
    updateStudentGroup,
    deleteStudentById,
};
