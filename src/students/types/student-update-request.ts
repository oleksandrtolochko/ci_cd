import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { IStudent } from './student.interface';

export interface IstudentUpdateRequest extends ValidatedRequestSchema {
    [ContainerTypes.Body]: Partial<IStudent>;
}
