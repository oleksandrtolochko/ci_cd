import ObjectID from 'bson-objectid';
import { HttpStatuses } from '../app/enums/http-statuses.enum';
import HttpExeption from '../app/exeptions/http-exeption';
import * as studentsModel from './students.model';
import * as groupModel from '../groups/group.model';
import { IStudent } from './types/student.interface';
import path from 'path';
import fs from 'fs/promises';

const getAllStudents = async () => {
    const students = await studentsModel.getAllStudents();
    const allGroups = await groupModel.getAllGroups();

    const studentsWithGroups = students.map((student) => {
        const group = allGroups.find((group) => group.id === student.groupId);

        return {
            ...student,
            groupId: group?.id || null,
            groupName: group?.name || null,
        };
    });

    return studentsWithGroups;
};

const getStudentById = async (studentId: string) => {
    const student = await studentsModel.getStudentById(studentId);
    const allGroups = await groupModel.getAllGroups();

    const group = allGroups.find((group) => group.id === student?.groupId);

    if (!student) {
        throw new HttpExeption(HttpStatuses.NOT_FOUND, 'Student not found');
    }
    return {
        ...student,
        groupId: group?.id || null,
        groupName: group?.name || null,
    };
};

const addNewStudent = (studentCreateSchema: Omit<IStudent, 'id'>) => {
    const student = studentsModel.addNewStudent(studentCreateSchema);
    return student;
};

const updateStudentById = async (
    studentId: string,
    studentUpdateSchema: Partial<IStudent>
) => {
    const student = await studentsModel.updateStudentById(
        studentId,
        studentUpdateSchema
    );
    if (!student) {
        throw new HttpExeption(HttpStatuses.NOT_FOUND, 'Student not found');
    }
    return student;
};

const addImage = async (id: string, filePath?: string) => {
    if (!filePath) {
        throw new HttpExeption(
            HttpStatuses.BAD_REQUEST,
            'File is not provided'
        );
    }

    try {
        const imageId = ObjectID().toHexString();

        const imageExtention = path.extname(filePath);

        const imageName = imageId + imageExtention;

        const studentsDirectoryName = 'students';
        const studentsDirectoryPath = path.join(
            __dirname,
            '../',
            'public/',
            studentsDirectoryName
        );
        const newImagePath = path.join(studentsDirectoryPath, imageName);
        const imagePath = `${studentsDirectoryName}/${imageName}`;

        await fs.rename(filePath, newImagePath);

        const updatedStudent = updateStudentById(id, { imagePath });
        return updatedStudent;
    } catch (error) {
        await fs.unlink(filePath);
        throw error;
    }
};

const deleteStudentById = async (studentId: string) => {
    const student = await studentsModel.deleteStudentById(studentId);
    if (!student) {
        throw new HttpExeption(HttpStatuses.NOT_FOUND, 'Student not found');
    }
    return student;
};

export {
    getAllStudents,
    getStudentById,
    addNewStudent,
    updateStudentById,
    addImage,
    deleteStudentById,
};
