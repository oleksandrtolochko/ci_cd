import express from 'express';
import * as studentsController from './students.controller';
import controllerWrapper from '../app/utils/controller-wrapper';
import validator from '../app/middlewares/validation.middlevare';
import { studentCreateSchema, studentUpdateSchema } from './student.schema';
import { idParamSchema } from '../app/schemas/id-param.schema';
import uploadMiddleware from '../app/middlewares/upload.middleware';

const router = express.Router();

router.get('/', controllerWrapper(studentsController.getAllStudents));
router.get(
    '/:id',
    validator.params(idParamSchema),
    controllerWrapper(studentsController.getStudentById)
);
router.post(
    '/',
    validator.body(studentCreateSchema),
    controllerWrapper(studentsController.addNewStudent)
);
router.patch(
    '/:id',
    validator.params(idParamSchema),
    validator.body(studentUpdateSchema),
    controllerWrapper(studentsController.updateStudentById)
);

router.patch(
    '/:id/image',
    validator.params(idParamSchema),
    uploadMiddleware.single('file'),
    controllerWrapper(studentsController.addImage)
);

router.patch(
    '/:id/group',
    validator.params(idParamSchema),
    controllerWrapper(studentsController.updateStudentGroup)
);

router.delete(
    '/:id',
    validator.params(idParamSchema),
    controllerWrapper(studentsController.deleteStudentById)
);

export default router;
