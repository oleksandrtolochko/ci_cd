import express from 'express';
import logger from 'morgan';
import cors from 'cors';
import studentsRouter from '../students/students.router';
import groupRouter from '../groups/group.router';
import exeptionsFilter from './middlewares/exeptions.filter';
import path from 'path';
// import auth from './middlewares/auth.middleware';

const app = express();

app.use(logger('tiny'));
app.use(cors());
app.use(express.json());
// app.use(auth);

const staticFilesPath = path.join(__dirname, '../', 'public');
app.use('/api/v1/public', express.static(staticFilesPath));

app.use('/api/v1/students', studentsRouter);
app.use('/api/v1/groups', groupRouter);

app.use(exeptionsFilter);

export default app;
