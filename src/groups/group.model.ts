import { IGroup } from './types/group.interface';
import ObjectID from 'bson-objectid';

export const groups: IGroup[] = [
    {
        id: ObjectID().toHexString(),
        name: 'IT-1',
    },
    {
        id: ObjectID().toHexString(),
        name: 'IT-2',
    },
    {
        id: ObjectID().toHexString(),
        name: 'IT-3',
    },
    {
        id: ObjectID().toHexString(),
        name: 'IT-4',
    },
    {
        id: ObjectID().toHexString(),
        name: 'IT-5',
    },
];

const getAllGroups = (): IGroup[] => {
    return groups;
};

const getGroupById = (groupId: string): IGroup | undefined => {
    const group = groups.find(({ id }) => id === groupId);
    return group;
};

const addNewGroup = (createGroupSchema: Omit<IGroup, 'id'>): IGroup => {
    const newGroup = {
        ...createGroupSchema,
        id: ObjectID().toHexString(),
    };
    groups.push(newGroup);
    return newGroup;
};

const updateGrouptById = (
    groupId: string,
    updateGroupSchema: Partial<IGroup>
): IGroup | undefined => {
    const groupIndex = groups.findIndex(({ id }) => id === groupId);
    const group = groups[groupIndex];

    if (!group) {
        return;
    }

    const updatedGroup = {
        ...group,
        ...updateGroupSchema,
    };

    groups.splice(groupIndex, 1, updatedGroup);

    return updatedGroup;
};

const deleteGroupById = (groupId: string): IGroup | undefined => {
    const groupIndex = groups.findIndex(({ id }) => id === groupId);
    const group = groups[groupIndex];

    if (!group) {
        return;
    }

    groups.splice(groupIndex, 1);

    return group;
};

export {
    getAllGroups,
    getGroupById,
    addNewGroup,
    updateGrouptById,
    deleteGroupById,
};
