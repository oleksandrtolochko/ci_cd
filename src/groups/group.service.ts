import { HttpStatuses } from '../app/enums/http-statuses.enum';
import HttpExeption from '../app/exeptions/http-exeption';
import * as groupModel from './group.model';
import * as studentsModel from '../students/students.model';
import { IGroup } from './types/group.interface';

const getAllGroups = async () => {
    const allGroups = await groupModel.getAllGroups();
    const allStudents = await studentsModel.getAllStudents();

    const groupsWithStudents = allGroups.map((group) => ({
        ...group,
        students: allStudents.filter((student) => student.groupId === group.id),
    }));

    return groupsWithStudents;
};

const getGroupById = async (groupId: string) => {
    const group = await groupModel.getGroupById(groupId);
    const allStudents = await studentsModel.getAllStudents();

    if (!group) {
        throw new HttpExeption(HttpStatuses.NOT_FOUND, 'Group not found');
    }
    return {
        ...group,
        students: allStudents.filter((student) => student.groupId === group.id),
    };
};

const addNewGroup = async (groupCreateSchema: Omit<IGroup, 'id'>) => {
    const group = await groupModel.addNewGroup(groupCreateSchema);
    return group;
};

const updateGroupById = async (
    groupId: string,
    groupUpdateSchema: Partial<IGroup>
) => {
    const group = await groupModel.updateGrouptById(groupId, groupUpdateSchema);
    if (!group) {
        throw new HttpExeption(HttpStatuses.NOT_FOUND, 'Group not found');
    }
    return group;
};

const deleteGroupById = async (groupId: string) => {
    const group = await groupModel.deleteGroupById(groupId);
    if (!group) {
        throw new HttpExeption(HttpStatuses.NOT_FOUND, 'Group not found');
    }
    return group;
};

export {
    getAllGroups,
    getGroupById,
    addNewGroup,
    updateGroupById,
    deleteGroupById,
};
