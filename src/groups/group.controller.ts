import express from 'express';
import * as groupService from './group.service';

const getAllGroups = async (req: express.Request, res: express.Response) => {
    const groups = await groupService.getAllGroups();

    res.status(200).json(groups);
};

const getGroupById = async (req: express.Request, res: express.Response) => {
    const groupId = req.params.id;
    const group = await groupService.getGroupById(groupId);

    res.status(200).json(group);
};

const addNewGroup = async (req: express.Request, res: express.Response) => {
    const data = req.body;
    const newGroup = await groupService.addNewGroup(data);
    res.status(201).json(newGroup);
};

const updateGrouptById = async (
    req: express.Request,
    res: express.Response
) => {
    const id = req.params.id;
    const data = req.body;
    const updatedGroup = await groupService.updateGroupById(id, data);
    res.status(200).json(updatedGroup);
};

const deleteGroupById = async (
    req: express.Request<{ id: string }>,
    res: express.Response
) => {
    const groupId = req.params.id;
    await groupService.deleteGroupById(groupId);
    res.status(200).json({ message: 'group successfully deleted' });
};

export {
    getAllGroups,
    getGroupById,
    addNewGroup,
    updateGrouptById,
    deleteGroupById,
};
