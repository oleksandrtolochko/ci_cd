import express from 'express';
import * as groupController from './group.controller';
import controllerWrapper from '../app/utils/controller-wrapper';
import validator from '../app/middlewares/validation.middlevare';
import { groupCreateSchema, groupUpdateSchema } from './group.schema';
import { idParamSchema } from '../app/schemas/id-param.schema';

const router = express.Router();

router.get('/', controllerWrapper(groupController.getAllGroups));
router.get(
    '/:id',
    validator.params(idParamSchema),
    controllerWrapper(groupController.getGroupById)
);
router.post(
    '/',
    validator.body(groupCreateSchema),
    controllerWrapper(groupController.addNewGroup)
);
router.patch(
    '/:id',
    validator.params(idParamSchema),
    validator.body(groupUpdateSchema),
    controllerWrapper(groupController.updateGrouptById)
);

router.delete(
    '/:id',
    validator.params(idParamSchema),
    controllerWrapper(groupController.deleteGroupById)
);

export default router;
